import unittest
from genName import generator

class TestgenName(unittest.TestCase):
    def test_generator(self):
        self.assertEqual(generator("Mont Louis","SuperVillain"),"The Dark Master")
        self.assertEqual(generator("MontLouis","SuperVillain"),"error")
        self.assertEqual(generator("étienne Champion","SuperVillain"),"error")
        self.assertEqual(generator("/à(-çè çàç&èn","SuperVillain","Custom MSG"),"Custom MSG")


    