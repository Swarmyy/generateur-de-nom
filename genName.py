"""
=========================================
Générateur de noms
=========================================
Author :MONT Louis
[#]_Note: only SuperVillain is implemented
"""

import string
from tkinter import *


def dicts():
    """ 
    Dictionaries   
    Returns the Dictionnary of String:Arrays of Dictionnaries of Char:String, complete list of all the universes.

    Returns: Dictionnary
    """
    dictionaries = {}
    SupervillainFN = {
        'A': 'The Evil',
        'B': 'The Mad',
        'C': 'The Big',
        'D': 'The Dangerous',
        'E': 'Captain',
        'F': 'The Ghostly',
        'G': 'Professor',
        'H': 'Doctor',
        'I': 'Phantom',
        'J': 'The Brutal',
        'K': 'The Unstoppable',
        'L': 'The Brutal',
        'M': 'The Dark',
        'N': 'The Crazy',
        'O': 'The Diabolic',
        'P': 'The Poison',
        'Q': 'The Crazed',
        'R': 'The Bloody',
        'S': 'The Dark',
        'T': 'The Dangerous',
        'U': 'The Rancid',
        'V': 'The Invisible',
        'W': 'The Black',
        'X': 'Diabolic',
        'Y': 'The Mega',
        'Z': 'The Grand'}
    SuperVillainLN = {
        'A': 'Shadow',
        'B': 'Knight',
        'C': 'Tarantula',
        'D': 'Skull',
        'E': 'Mastermind',
        'F': 'Wizard',
        'G': 'Ninja',
        'H': 'Devil',
        'I': 'Freak',
        'J': 'Beast',
        'K': 'Criminal',
        'L': 'Master',
        'M': 'Menace',
        'N': 'Child',
        'O': 'Corpse',
        'P': 'Slayer',
        'Q': 'Spider',
        'R': 'Creature',
        'S': 'Abomination',
        'T': 'Monster',
        'U': 'Vampire',
        'V': 'Mutant',
        'W': 'Robot',
        'X': 'Robot',
        'Y': 'Machine',
        'Z': 'Clown'}
    arraySV = (SupervillainFN, SuperVillainLN)
    #Template is Male Names
    GoTFN = {
        'A': 'Davos',
        'B': 'Viserys',
        'C': 'Brandon',
        'D': 'Loras',
        'E': 'Edd',
        'F': 'Aegon',
        'G': 'Gendry',
        'H': 'Tywin',
        'I': 'Ilyn',
        'J': 'Hallyne',
        'K': 'Pycelle',
        'L': 'Tyrion',
        'M': 'Cressen',
        'N': 'Rickon',
        'O': 'Jamie',
        'P': 'Samwell',
        'Q': 'Qyburn',
        'R': 'Barristan',
        'S': 'Victarion',
        'T': 'Waymar',
        'U': 'Yoren',
        'V': 'Robert',
        'W': 'Renly',
        'X': 'Kevan',
        'Y': 'Lommy',
        'Z': 'Jojen'}
    GotLN = {
        'A': 'Dondarrion',
        'B': 'Clegane',
        'C': 'Greyjoy',
        'D': 'Snow',  # sorry you're a bastard
        'E': 'Baratheon',
        'F': 'Tarly',
        'G': 'Martell',
        'H': 'Arryn',
        'I': 'Locke',
        'J': 'Tallhart',
        'K': 'Frey',
        'L': 'Stark',
        'M': 'Royce',
        'N': 'Tyrell',
        'O': 'Bolton',
        'P': 'Targaryen',
        'Q': 'Darry',
        'R': 'Harlaw',
        'S': 'Tully',
        'T': 'Lannister',
        'U': 'of Asshai',  # original Yoren but didn't to create a redundancy
        'V': 'Sunderly',
        'W': 'Westerling',
        'X': 'Mormont',
        'Y': 'Ashford',
        'Z': 'Karstark'}
    arrayGoT = (GoTFN, GotLN)
    #Template is Male Names
    StarCraftFN = {
        'A': 'Jim',
        'B': 'Marlon',
        'C': 'Kinnel',
        'D': 'Mather',
        'E': 'Channing',
        'F': 'Louis',
        'G': 'Miles',
        'H': 'Adriel',
        'I': 'Andy',
        'J': 'Bradney',
        'K': 'Arcturus',
        'L': 'Kent',
        'M': 'Parry',
        'N': 'Ned',
        'O': 'Nelly',
        'P': 'Crawford',
        'Q': 'Roderick',
        'R': 'Burns',
        'S': 'Carden',
        'T': 'Stevenson',
        'U': 'Adan',
        'V': 'Edgar',
        'W': 'Sarah',
        'X': 'Nathaniel',
        'Y': 'Rawls',
        'Z': 'Tychus'}
    StarcraftLN = {
        'A': 'Raynor',
        'B': 'Birkenhead',
        'C': 'Twynam',
        'D': 'Camden',
        'E': 'Channing',
        'F': 'Adlam',
        'G': 'Dayton',
        'H': 'Wheatley',
        'I': 'Dudley',
        'J': 'Gresham',
        'K': 'Bradney',
        'L': 'Mengsk',
        'M': 'Richmond',
        'N': 'Stamper',
        'O': 'Smyth',
        'P': 'Fletcher',
        'Q': 'Compton',
        'R': 'Langley',
        'S': 'Smither',
        'T': 'Stevenson',
        'U': 'Bunce',
        'V': 'Gresham',
        'W': 'Kerrigan',
        'X': 'Hayden',
        'Y': 'Foy',
        'Z': 'Findlay'}
    arraySC = (StarCraftFN, StarcraftLN)
    dictionaries['SuperVillain'] = arraySV
    dictionaries['Game Of Thrones'] = arrayGoT
    dictionaries['StarCraft'] = arraySC
    return dictionaries


def generator(names, dictName, errorMsg='error'):
    """
    Generates the complete name.

    Parameters:
        names: The inputted FirstName and LastName, separated by a space
        dictNames: The name of the universes we are getting the names in
        errorMsg: The Error Message if something goes wrong, 'error' by default

    Returns:
        string: The complete name according those inputted in the selected setting, or errorMsg
    """
    dictionary = dicts()[dictName]
    try:
        FN = str.split(names, ' ')[0][0].upper()
        LN = str.split(names, ' ')[1][0].upper()
    except:
        return errorMsg
    if dictionary[0].get(FN, errorMsg) == errorMsg or dictionary[1].get(LN, errorMsg) == errorMsg:
        return errorMsg
    return str.format("%s %s" % (dictionary[0].get(FN, errorMsg), dictionary[1].get(LN, errorMsg)))


def callback_generator(names, dictionnary, errorMsg='error'):
    """
    Displaying when clicking the button.
    Displays the complete name according those inputted in the selected setting, or errorMsg.
    Parameters:
        names: The inputted FirstName and LastName, separated by a space
        dictNames: The name of the universes we are getting the names in
        errorMsg: The Error Message if something goes wrong, "error" by default    
    """
    result.delete('1.0', END)
    result.insert(END, generator(names, dictionnary, errorMsg))


"""
Main
"""

errorMsg = 'Veuillez respecter le format Nom Prénom séparés par un espace sans accents, ni caractères spéciaux'

frame = Tk()
frame.title('Générateur de Noms')
frame.minsize(400, 300)
# cols
frame.columnconfigure(0, weight=1)
frame.columnconfigure(1, weight=2)
frame.columnconfigure(2, weight=1)
# rows
frame.rowconfigure(0, weight=1)
frame.rowconfigure(1, weight=1)
frame.rowconfigure(2, weight=4)

names = Entry(frame)
names.insert(0, 'Firstname Lastname')
names.insert(END, '')
names.grid(row=0, column=1)

result = Text(frame, wrap='word')
result.insert(END, "")
result.grid(row=2, column=0, columnspan=3, sticky='nsew')

listSettings = ('SuperVillain', 'Game Of Thrones', 'StarCraft')
selectedSetting = StringVar()
selectedSetting.set(listSettings[0])
Settings = OptionMenu(frame, selectedSetting, *listSettings)
Settings.grid(row=0, column=2)

btn_generator = Button(frame, text='Génération du nom', command=lambda: callback_generator(
    names.get(), selectedSetting.get(), errorMsg))
btn_generator.grid(row=1, column=1)

frame.mainloop()
